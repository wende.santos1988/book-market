package com.mercadolivro.exception

import org.springframework.http.HttpStatus

class BookStatusNotPermittedException(message: String?, val httpStatus: HttpStatus): RuntimeException(message)