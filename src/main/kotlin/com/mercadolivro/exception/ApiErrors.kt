package com.mercadolivro.exception

import lombok.Data
import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError
import org.springframework.web.server.ResponseStatusException

@Data
class ApiErrors {

    private var errors: MutableList<ErrorResponse> = ArrayList()

    constructor(fieldsErrors: List<FieldError>, httpStatus: HttpStatus) {
        fieldsErrors.forEach {
            val rejectedValue = it.rejectedValue ?: ""
            val defaultMessage = "${it.field} $rejectedValue ${it.defaultMessage}"
            errors.add(generateErrorResponse(httpStatus, defaultMessage))
        }
    }

    constructor(businessException: BusinessException) {
        businessException.message?.let {
            val message = businessException.message
            errors.add(generateErrorResponse(businessException.httpStatus, message))
        }
    }

    constructor(responseStatusException: ResponseStatusException) {
        responseStatusException.reason?.let {
            val message = responseStatusException.reason
            errors.add(generateErrorResponse(responseStatusException.status, message!!))
        }
    }

    constructor(invalidCustomerException: InvalidCustomerException){
        invalidCustomerException.message?.let {
            val message = invalidCustomerException.message
            errors.add(generateErrorResponse(invalidCustomerException.httpStatus, message))
        }
    }

    constructor(bookStatusInvalidException: BookStatusNotPermittedException){
        bookStatusInvalidException.message?.let {
            val message = bookStatusInvalidException.message
            errors.add(generateErrorResponse(bookStatusInvalidException.httpStatus, message))
        }
    }

    constructor(notFoundException: NotFoundException){
        notFoundException.message?.let {
            val message = notFoundException.message
            errors.add(generateErrorResponse(notFoundException.httpStatus, message))
        }
    }

    fun getErrors(): List<ErrorResponse?> {
        return errors
    }

    private fun generateErrorResponse(httpStatus: HttpStatus, message: String): ErrorResponse {
        return ErrorResponse(httpStatus.value(), message)
    }

}