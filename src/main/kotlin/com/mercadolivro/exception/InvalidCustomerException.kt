package com.mercadolivro.exception

import org.springframework.http.HttpStatus

class InvalidCustomerException(message: String?, val httpStatus: HttpStatus): RuntimeException(message) {

}