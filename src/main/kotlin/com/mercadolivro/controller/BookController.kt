package com.mercadolivro.controller

import com.mercadolivro.domain.dto.request.CreateBookDTO
import com.mercadolivro.domain.dto.request.UpdateBookDOT
import com.mercadolivro.domain.dto.response.BookResponseDTO
import com.mercadolivro.domain.enums.ErrorsMessagesEnum
import com.mercadolivro.domain.facade.book.UpdateBookFacade
import com.mercadolivro.domain.service.BookService
import com.mercadolivro.domain.service.CustomerService
import com.mercadolivro.exception.NotFoundException
import com.mercadolivro.extension.convertToDto
import com.mercadolivro.extension.convertToModel
import lombok.RequiredArgsConstructor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequiredArgsConstructor
class BookController(
    val bookService: BookService,
    val customerService: CustomerService,
    val updateBookFacade: UpdateBookFacade
) {


    object UrlConstants {
        const val BASE_URL: String = "/book"
        const val POST_CREATE_BOOK: String = BASE_URL
        const val FIND_ACTIVES_BOOK: String = "$BASE_URL/actives"
        const val FIND_BOOK_BY_ID: String = "$BASE_URL/{id}"
        const val PUT_UPDATE_BOOK_BY_ID: String = "$BASE_URL/{id}"
        const val DELETE_BOOK_BY_ID: String = "$BASE_URL/{id}"
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(UrlConstants.POST_CREATE_BOOK)
    fun create(@RequestBody @Valid request: CreateBookDTO): BookResponseDTO {
        val customer = customerService
            .findCustomerById(request.customerId!!.toLong())
            .orElseThrow { NotFoundException(
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.message.format(request.customerId.toLong()),
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.httpStatus
            )}

        return bookService.create(request.convertToModel(customer)).convertToDto()
    }

    @GetMapping(UrlConstants.BASE_URL)
    fun findAllBooks(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<BookResponseDTO> =
        bookService.findAllBooks(pageable).map { it.convertToDto() }


    @GetMapping(UrlConstants.FIND_ACTIVES_BOOK)
    fun findAllBooksActives(@PageableDefault(page = 0, size = 10) pageable: Pageable): Page<BookResponseDTO> =
        bookService.findAllBooksActives(pageable).map { it.convertToDto() }


    @GetMapping(UrlConstants.FIND_BOOK_BY_ID)
    fun findBookById(@PathVariable id: Long): BookResponseDTO =
        bookService.findBookById(id).convertToDto()


    @PutMapping(UrlConstants.PUT_UPDATE_BOOK_BY_ID)
    fun updateBookById(@PathVariable id: Long, @RequestBody updateBookDOT: UpdateBookDOT): BookResponseDTO =
        updateBookFacade.updateBook(id, updateBookDOT)

}