package com.mercadolivro.controller

import com.mercadolivro.domain.dto.request.CreateCustomerDTO
import com.mercadolivro.domain.dto.request.UpdateCustomerDOT
import com.mercadolivro.domain.dto.response.CustomerResponseDTO
import com.mercadolivro.domain.enums.ErrorsMessagesEnum
import com.mercadolivro.domain.facade.customer.FindCustomerByIdFacade
import com.mercadolivro.extension.convertToModel
import com.mercadolivro.domain.service.CustomerService
import com.mercadolivro.exception.BusinessException
import com.mercadolivro.exception.NotFoundException
import com.mercadolivro.extension.convertToResponse
import lombok.RequiredArgsConstructor
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import javax.validation.Valid

@RestController
@RequiredArgsConstructor
class CustomerController(
    val customerService: CustomerService,
    val findCustomerByIdFacade: FindCustomerByIdFacade
) {

    object UrlConstants {
        const val BASE_URL: String = "/customer"
        const val POST_CREATE_CUSTOMER: String = BASE_URL
        const val GET_FIND_BY_ID: String = "$BASE_URL/{id}"
        const val PUT_UPDATE_BY_ID: String = "$BASE_URL/{id}"
        const val DELETE_BY_ID: String = "$BASE_URL/{id}"
    }

    @GetMapping(UrlConstants.GET_FIND_BY_ID)
    fun findCustomerById(@PathVariable id: Long): CustomerResponseDTO =
        findCustomerByIdFacade.findCustomerById(id)
            .orElseThrow {
                NotFoundException(
                    ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.message.format(id),
                    ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.httpStatus
                )}
            .convertToResponse()


    @GetMapping(UrlConstants.BASE_URL)
    fun findAllCustomer(@RequestParam name: String?): List<CustomerResponseDTO>? =
        customerService.findAllCustomer(name).map { it.convertToResponse() }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(UrlConstants.POST_CREATE_CUSTOMER)
    fun create(@RequestBody @Valid createCustomerDTO: CreateCustomerDTO): CustomerResponseDTO =
        customerService.create(createCustomerDTO.convertToModel()).convertToResponse()


    @ResponseStatus(HttpStatus.OK)
    @PutMapping(UrlConstants.PUT_UPDATE_BY_ID)
    fun update(@PathVariable id: Long, @RequestBody customerDtoUpdate: UpdateCustomerDOT): CustomerResponseDTO {
        val customerSaved = customerService.findCustomerById(id)
            .orElseThrow { NotFoundException(
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.message.format(id),
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.httpStatus
            )}

        return customerDtoUpdate.convertToModel(customerSaved).convertToResponse()
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(UrlConstants.DELETE_BY_ID)
    fun delete(@PathVariable id: Long) = customerService.delete(id)



}