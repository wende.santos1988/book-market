package com.mercadolivro.controller.mapper

import com.mercadolivro.domain.dto.request.PostPurchaseRequest
import com.mercadolivro.domain.entities.Purchase
import com.mercadolivro.domain.facade.customer.FindCustomerByIdFacade
import com.mercadolivro.domain.service.BookService
import org.springframework.stereotype.Component

@Component
class PurchaseMapper(
    private val findCustomerByIdFacade: FindCustomerByIdFacade,
    private val bookService: BookService
) {

    fun toModel(request: PostPurchaseRequest): Purchase {
        val customerById = findCustomerByIdFacade.findCustomerById(request.customerId)
        val books = bookService.findBookByIds(request.bookIds)

        return Purchase(
            customer = customerById.get(),
            books = books,
            price = books.sumOf { it.price }
        )
    }

}