package com.mercadolivro.domain.facade.customer

import com.mercadolivro.domain.entities.Customer
import com.mercadolivro.domain.enums.CustomerStatus
import com.mercadolivro.domain.enums.ErrorsMessagesEnum
import com.mercadolivro.domain.service.CustomerService
import com.mercadolivro.exception.NotFoundException
import org.springframework.stereotype.Service
import java.util.*

@Service
class FindCustomerByIdFacade(
    val customerService: CustomerService
) {

    fun findCustomerById(id: Long): Optional<Customer> {
        val customerSaved = customerService.findCustomerById(id)
        if (!customerSaved.isEmpty && customerSaved.get().status == CustomerStatus.INACTIVE) {
            throw NotFoundException(
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.message.format(id),
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.httpStatus
            )
        }
        return customerSaved;
    }

}