package com.mercadolivro.domain.facade.book

import com.mercadolivro.domain.dto.request.UpdateBookDOT
import com.mercadolivro.domain.dto.response.BookResponseDTO
import com.mercadolivro.domain.enums.BookStatus
import com.mercadolivro.domain.enums.ErrorsMessagesEnum
import com.mercadolivro.domain.service.BookService
import com.mercadolivro.exception.BookStatusNotPermittedException
import com.mercadolivro.extension.convertToDto
import com.mercadolivro.extension.convertToModel
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class UpdateBookFacade(
    val bookService: BookService,
) {

    fun updateBook(id: Long, updateBookDOT: UpdateBookDOT): BookResponseDTO {
        if (updateBookDOT.status == BookStatus.DELETED || updateBookDOT.status == BookStatus.CANCELED) {
            throw BookStatusNotPermittedException(
                ErrorsMessagesEnum.BOOK_STATUS_NOT_PERMITTED.message.format(updateBookDOT.status),
                ErrorsMessagesEnum.BOOK_STATUS_NOT_PERMITTED.httpStatus
            )
        }

        val bookSaved = bookService.findBookById(id)
        val bookToUpdate = updateBookDOT.convertToModel(bookSaved)
        return bookService.update(bookToUpdate).convertToDto()
    }

}