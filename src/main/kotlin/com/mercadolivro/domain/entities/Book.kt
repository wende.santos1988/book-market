package com.mercadolivro.domain.entities

import com.mercadolivro.domain.enums.BookStatus
import java.math.BigDecimal
import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY

@Entity
data class Book(

    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,

    @Column
    var name: String,

    @Column
    var price: BigDecimal,

    @ManyToOne
    @JoinColumn(name = "customer_id")
    var customer: Customer? = null
) {

    @Column
    @Enumerated(EnumType.STRING)
    var status: BookStatus? = null

    constructor(
        id: Long? = null,
        name: String,
        price: BigDecimal,
        status: BookStatus?,
        customer: Customer? = null,
    ): this(id, name, price, customer) {
        this.status = status
    }
}