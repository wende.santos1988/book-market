package com.mercadolivro.domain.entities

import com.mercadolivro.domain.enums.CustomerStatus
import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY

@Entity
data class Customer(

    @Id
    @GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,

    @Column
    var name: String,

    @Column
    var email: String,

    @Column
    @Enumerated(EnumType.STRING)
    var status: CustomerStatus
) {
}