package com.mercadolivro.domain.enums

enum class CustomerStatus {
    ACTIVE,
    INACTIVE
}
