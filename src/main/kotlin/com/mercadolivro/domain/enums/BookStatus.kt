package com.mercadolivro.domain.enums

enum class BookStatus {

    ACTIVE,
    SOLD,
    CANCELED,
    DELETED

}