package com.mercadolivro.domain.dto.response

import com.mercadolivro.domain.entities.Customer
import com.mercadolivro.domain.enums.BookStatus
import java.math.BigDecimal

class BookResponseDTO(
    var id: Long,
    var name: String,
    var price: BigDecimal,
    var status: BookStatus,
    var customer: Customer
)