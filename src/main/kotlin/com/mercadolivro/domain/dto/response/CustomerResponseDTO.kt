package com.mercadolivro.domain.dto.response

import com.mercadolivro.domain.enums.CustomerStatus

data class CustomerResponseDTO(

    val id: Long? = null,
    val name: String,
    val email: String,
    val status: CustomerStatus
)
