package com.mercadolivro.domain.dto.request

import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class PostPurchaseRequest(
    @field: NotNull
    @field: Positive
    val customerId: Long,

    @field: NotNull
    val bookIds: Set<Long>



)