package com.mercadolivro.domain.dto.request

import com.fasterxml.jackson.annotation.JsonAlias
import java.math.BigDecimal
import javax.validation.constraints.NotNull

class CreateBookDTO (
    @field: NotNull var name: String?,
    @field: NotNull var price: BigDecimal?,

    @JsonAlias("customer_id")
    @field: NotNull val customerId: Int?
)
