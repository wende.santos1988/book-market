package com.mercadolivro.domain.service

import com.mercadolivro.domain.entities.Customer
import com.mercadolivro.domain.enums.CustomerStatus
import com.mercadolivro.domain.enums.CustomerStatus.*
import com.mercadolivro.domain.enums.ErrorsMessagesEnum
import com.mercadolivro.domain.repository.CustomerRepository
import com.mercadolivro.exception.BusinessException
import com.mercadolivro.exception.NotFoundException
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service
import java.util.Optional

@Service
@RequiredArgsConstructor
class CustomerService(
    val customerRepository: CustomerRepository,
    val bookService: BookService
) {

    fun findCustomerById(id: Long): Optional<Customer> {
        return customerRepository.findById(id)
    }

    fun findAllCustomer(name: String?): List<Customer> {
        name?.let {
            return customerRepository.findByName(name)
        }
        return customerRepository.findAll().toList()
    }

    fun create(customer: Customer): Customer {
        if (customerRepository.existsByEmail(customer.email)) {
            throw BusinessException(
                ErrorsMessagesEnum.EMAIL_ALREADY_EXISTS.message.format(customer.email),
                ErrorsMessagesEnum.EMAIL_ALREADY_EXISTS.httpStatus
            )
        }
        return customerRepository.save(customer)
    }

    fun update(customerToUpdate: Customer): Customer {
        if (!customerRepository.existsById(customerToUpdate.id!!)) {
            throw NotFoundException(
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.message.format(customerToUpdate.id),
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.httpStatus
            )
        }
        return customerRepository.save(customerToUpdate)
    }

    fun delete(id: Long) {
        val customer = findCustomerById(id)
            .orElseThrow { NotFoundException(
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.message.format(id),
                ErrorsMessagesEnum.CUSTOMER_NOT_FOUND.httpStatus
            )}

        bookService.deleteByCustomer(customer)
        customer.status = INACTIVE
        update(customer)

//        customerRepository.deleteById(id)
    }

    fun emailIsAvailable(value: String): Boolean {
        return !customerRepository.existsByEmail(value)
    }

}