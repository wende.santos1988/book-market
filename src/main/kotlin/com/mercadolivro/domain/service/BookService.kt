package com.mercadolivro.domain.service

import com.mercadolivro.domain.entities.Book
import com.mercadolivro.domain.entities.Customer
import com.mercadolivro.domain.enums.BookStatus
import com.mercadolivro.domain.enums.ErrorsMessagesEnum
import com.mercadolivro.domain.repository.BookRepository
import com.mercadolivro.exception.NotFoundException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class BookService(
    val repository: BookRepository
) {
    fun create(book: Book): Book {
        return repository.save(book)
    }

    fun findAllBooks(pageable: Pageable): Page<Book> {
        return repository.findAll(pageable)
    }

    fun findAllBooksActives(pageable: Pageable): Page<Book> {
        return repository.findByStatus(BookStatus.ACTIVE, pageable)
    }

    fun findBookById(id: Long): Book {
        return repository.findById(id)
            .orElseThrow{
                NotFoundException(
                    ErrorsMessagesEnum.BOOK_NOT_FOUND.message.format(id),
                    ErrorsMessagesEnum.BOOK_NOT_FOUND.httpStatus
            )}
    }

    fun update(book: Book): Book {
        return repository.save(book);
    }

    fun deleteByCustomer(customer: Customer) {
        val books = repository.findByCustomer(customer)
        for (book in books) {
            book.status = BookStatus.DELETED
        }
        repository.saveAll(books)
    }

    fun findBookByIds(bookIds: Set<Long>): List<Book> {
        return repository.findAllById(bookIds).toList()
    }
}
