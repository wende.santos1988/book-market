package com.mercadolivro.domain.repository

import com.mercadolivro.domain.entities.Book
import com.mercadolivro.domain.entities.Customer
import com.mercadolivro.domain.enums.BookStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BookRepository: JpaRepository<Book, Long> {
    fun findByStatus(active: BookStatus, pageable: Pageable): Page<Book>
    fun findByCustomer(customer: Customer): List<Book>
}