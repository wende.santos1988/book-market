package com.mercadolivro.extension

import com.mercadolivro.domain.dto.request.CreateBookDTO
import com.mercadolivro.domain.dto.request.CreateCustomerDTO
import com.mercadolivro.domain.dto.request.UpdateBookDOT
import com.mercadolivro.domain.dto.request.UpdateCustomerDOT
import com.mercadolivro.domain.dto.response.BookResponseDTO
import com.mercadolivro.domain.dto.response.CustomerResponseDTO
import com.mercadolivro.domain.entities.Book
import com.mercadolivro.domain.entities.Customer
import com.mercadolivro.domain.enums.BookStatus
import com.mercadolivro.domain.enums.CustomerStatus.ACTIVE

fun CreateCustomerDTO.convertToModel(): Customer {
    return Customer(name = this.name!!, email = this.email!!, status = ACTIVE)
}

fun UpdateCustomerDOT.convertToModel(customerSaved: Customer): Customer {
    return Customer(id = customerSaved.id, name = this.name, email = this.email, status = customerSaved.status)
}

fun Customer.convertToResponse(): CustomerResponseDTO {
    return CustomerResponseDTO(id = this.id, name = this.name, email = this.email, status = this.status)
}

fun CreateBookDTO.convertToModel(customer: Customer): Book {
    return Book(name = this.name!!, price = this.price!!, status = BookStatus.ACTIVE, customer = customer)
}

fun Book.convertToDto(): BookResponseDTO {
    return BookResponseDTO(this.id!!, this.name, this.price, this.status!!, this.customer!!)
}

fun UpdateBookDOT.convertToModel(previousValue: Book): Book {
    return Book(
        id = previousValue.id,
        name = previousValue.name ?: this.name,
        price = previousValue.price ?: this.price,
        status = previousValue.status,
        customer = previousValue.customer
    )
}

