package com.mercadolivro.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.mercadolivro.domain.dto.request.CreateCustomerDTO
import com.mercadolivro.domain.entities.Customer
import com.mercadolivro.domain.enums.CustomerStatus.*
import com.mercadolivro.domain.enums.ErrorsMessagesEnum
import com.mercadolivro.domain.facade.customer.FindCustomerByIdFacade
import com.mercadolivro.domain.service.CustomerService
import com.mercadolivro.exception.BusinessException
import com.mercadolivro.extension.convertToModel
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.mockito.BDDMockito.*
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.util.*

@Extensions(ExtendWith(SpringExtension::class))
@ActiveProfiles("test")
@WebMvcTest(controllers = [CustomerController::class])
@AutoConfigureWebMvc
internal class CustomerControllerTest @Autowired constructor(private val mockMvc: MockMvc) {

    private val CUSTOMER_API: String = "/customer"

    @MockBean
    lateinit var customerService: CustomerService

    @MockBean
    lateinit var findCustomerByIdFacade: FindCustomerByIdFacade

    @BeforeEach
    fun setUp() {
    }

    @Test
    fun `should create customer ith success`() {
        val customerRequestValues = CreateCustomerDTO(name = "Wendel Santos", email = "test@ndo.com")
        val customerToCreation = customerRequestValues.convertToModel()
        val customerSaved = Customer(
            id = 1L,
            name = customerToCreation.name,
            email = customerToCreation.email,
            status = customerToCreation.status)

        given(customerService.create(customerToCreation)).willReturn(customerSaved)
        given(customerService.emailIsAvailable(customerToCreation.email)).willReturn(true)
        val json: String = ObjectMapper().writeValueAsString(customerRequestValues)

        val request: MockHttpServletRequestBuilder = MockMvcRequestBuilders
            .post(CUSTOMER_API)
            .contentType(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .content(json)

        mockMvc.perform(request)
            .andExpect(status().isCreated)
            .andExpect(jsonPath("id").isNotEmpty)
            .andExpect(jsonPath("name").value(customerRequestValues.name))
            .andExpect(jsonPath("email").value(customerRequestValues.email))
    }

    @Test
    fun `should throw an error when the fields provided are not enough to create the customer`() {
        val json: String = ObjectMapper().writeValueAsString(CreateCustomerDTO(null, null))

        val request: MockHttpServletRequestBuilder = MockMvcRequestBuilders
            .post(CUSTOMER_API)
            .contentType(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .content(json)

        mockMvc
            .perform(request)
            .andExpect(status().isUnprocessableEntity)
            .andExpect(jsonPath("errors", hasSize<Int>(2)))
    }

    @Test
    fun `should throw an error when trying to create a customer with duplicate email`() {
        val customerRequestValues = CreateCustomerDTO(name = "Wendel Santos", email = "test@ndo.com")
        val json: String = ObjectMapper().writeValueAsString(customerRequestValues)
        val message = ErrorsMessagesEnum.EMAIL_ALREADY_EXISTS

        given(customerService.emailIsAvailable(customerRequestValues.email!!)).willReturn(true)
        given(customerService.create(customerRequestValues.convertToModel()))
            .willThrow(BusinessException(message.message.format(customerRequestValues.email), message.httpStatus))

        val request: MockHttpServletRequestBuilder = MockMvcRequestBuilders
            .post(CUSTOMER_API)
            .contentType(APPLICATION_JSON)
            .accept(APPLICATION_JSON)
            .content(json)

        mockMvc
            .perform(request)
            .andExpect(status().isBadRequest)
            .andExpect(jsonPath("errors", hasSize<Int>(1)))
            .andExpect(jsonPath("errors[0].message").value(message.message.format(customerRequestValues.email)))

    }

    @Test
    fun `should get information a customer`() {
        val id = 1L
        val customerCreateDTO = CreateCustomerDTO(name = "Wendel Santos", email = "test@ndo.com")
        val customerToSave = customerCreateDTO.convertToModel()
        customerToSave.id = id

        given(findCustomerByIdFacade.findCustomerById(id)).willReturn(Optional.of(customerToSave))
        given(customerService.findCustomerById(id)).willReturn(Optional.of(customerToSave))

        val request: MockHttpServletRequestBuilder = MockMvcRequestBuilders
            .get("$CUSTOMER_API/$id")
            .accept(APPLICATION_JSON)

        mockMvc.perform(request)
            .andExpect(status().isOk)
            .andExpect(jsonPath("id").value(id))
            .andExpect(jsonPath("name").value(customerCreateDTO.name))
            .andExpect(jsonPath("email").value(customerCreateDTO.email))
            .andExpect(jsonPath("status").value(ACTIVE.toString()))
    }

    @Test
    fun `should return resource not found when book not exists`() {
        val id = 1L

        given(customerService.findCustomerById(id)).willReturn(Optional.empty())

        val request: MockHttpServletRequestBuilder = MockMvcRequestBuilders
            .get("$CUSTOMER_API/$id")
            .accept(APPLICATION_JSON)

        mockMvc
            .perform(request)
            .andExpect(status().isNotFound)
    }

    @Test
    fun `should delete book`() {
        val customerSaved = Customer(
            id = 1L,
            name = "Wendel Santos",
            email = "test@ndo.com",
            status = ACTIVE)

        given(customerService.findCustomerById(Mockito.anyLong())).willReturn(Optional.of(customerSaved))

        val request: MockHttpServletRequestBuilder = MockMvcRequestBuilders
            .delete("$CUSTOMER_API/${customerSaved.id}")

        mockMvc
            .perform(request)
            .andExpect(status().isNoContent)
    }

}